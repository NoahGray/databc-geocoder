# B.C. Physical Address Geocoder Node Client

This is a simple client for Data BC's geocoding service. It's an unofficial client and not endorsed by the government of British Columbia.

## Installation

In your ternminal:

```zsh
yarn add databc-geocoder
# OR
npm install --save databc-geocoder
```

## Usage

See `./src/index.test.mjs` for for examples.

```js
const geocoder = new Geocoder({
  accessToken: 'YOUR-API-KEY',
  outputFormat: 'json' // optional, json is default
})

geocoder.addresses({
  addressString: '1917 Ferndale',
  bbox: '-123.308527, 49.004921, -122.081220, 49.394692'
})
.then((data) => {
  // do what you like with the data. Returns JSON by default.
})
```

## API

Below is a list of supported functions. See the parameters through the interactive [console](https://catalogue.data.gov.bc.ca/dataset/physical-address-geocoding-web-service/resource/40d6411e-ab98-4df9-a24e-67f81c45f6fa) or [JSON spec](https://oas-editor.apps.gov.bc.ca/?url=https://raw.githubusercontent.com/bcgov/api-specs/master/geocoder/geocoder-combined.json)

### client.addresses
        
### client.addresses.nearest
### client.addresses.near
### client.addresses.within

### client.intersections.nearest
### client.intersections.near
### client.intersections.within

### client.occupants.nearest
### client.occupants.near
### client.occupants.within

## TODO

Needs support for:

`/sites/{siteID}.{outputFormat}`
`/intersections/{intersectionID}.{outputFormat}`
`/occupants/{occupantID}.{outputFormat}`
`/parcels/pids/{siteID}.{outputFormat}`
