// @flow

import fetch from 'node-fetch'

export default class BCMapsClient {
  constructor ({ accessToken = null, outputFormat = 'json'}: Object<{ accessToken: string, outputFormat?: string }>) {
    this.accessToken = accessToken
    this.outputFormat = outputFormat
    this.hostUrl = 'https://geocoder.api.gov.bc.ca'

    this.endpoints = [
      'addresses', { 
        sites: ['nearest', 'near', 'within']
      }, {
        intersections: ['nearest', 'near', 'within']
      }, {
        occupants: ['addresses','nearest', 'near', 'within']
      }
    ]

    this.endpoints.forEach((endpoint) => {
      if (typeof endpoint === 'string') {
        this[endpoint] = async (options) => {
          const url = new URL(`${this.hostUrl}/${endpoint}.${this.outputFormat}`)

          for (const key in options) {
            url.searchParams.set(key, options[key])
          }

          console.log('url', url.href)

          const result = await fetch(url, {
            headers: {
              apiKey: this.accessToken
            }
          })

          if (this.outputFormat === 'json') {
            return result.json()
          }
          
          return result.text()
        }

        return
      }

      const parent = Object.keys(endpoint)[0]

      this[parent] = {}

      endpoint[parent].forEach((each) => {
        this[parent][each] = async (options) => {
          const url = new URL(`${this.hostUrl}/${parent}/${each}.${this.outputFormat}`)

          for (const key in options) {
            url.searchParams.set(key, options[key])
          }

          const result = await fetch(url, {
            headers: {
              apiKey: this.accessToken
            }
          })

          if (this.outputFormat === 'json') {
            return result.json()
          }
          
          return result.text()
        }

        return
      })
    })
  }
}
