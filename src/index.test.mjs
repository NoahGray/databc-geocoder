import test from 'tape'
import Geocoder from './index'

const geocoder = new Geocoder({
  accessToken: process.env.BC_MAPS_TOKEN
})

test('creating client', async (t) => {
  t.ok(geocoder instanceof Geocoder, 'client should be instance of class')
  t.ok(typeof geocoder.addresses === 'function', 'should have addresses function')
  t.ok(typeof geocoder.sites === 'object', 'should have sites object')
  t.ok(typeof geocoder.intersections === 'object', 'should have intersections object')
  t.ok(typeof geocoder.occupants === 'object', 'should have occupants object')
  t.end()
})

test('addresses function', async (t) => {
  const addresses = await geocoder.addresses({
    addressString: '1917 Ferndale',
    bbox: '-123.308527, 49.004921, -122.081220, 49.394692'
  })

  t.ok(typeof addresses === 'object', 'should return object')
  t.ok(Array.isArray(addresses.features), 'should have features array')
  t.equals(addresses.features[0].properties.fullAddress.toLowerCase(), '1917 ferndale st, vancouver, bc')
  t.end()
})


test('occupants function', async (t) => {
  const addresses = await geocoder.occupants.addresses({
    autoComplete: true,
    addressString: 'cannabis',
    bbox: '-123.308527, 49.004921, -122.081220, 49.394692',
    matchPrecisionNot: 'PROVINCE',
    matchPrecision: 'SITE',
  })

  t.ok(typeof addresses === 'object', 'should return object')
  t.ok(Array.isArray(addresses.features), 'should have features array')
  t.equals(addresses.features[0].properties.fullAddress.toLowerCase(), '1917 ferndale st, vancouver, bc')
  t.end()
})

